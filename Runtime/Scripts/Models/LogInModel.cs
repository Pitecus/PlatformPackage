using System;
using UnityEditor;
using UnityEngine;

namespace PlatformRequest
{
    public static class LogInModel
    {
        private static string uri = "https://genesys-security-svc-dev.azurewebsites.net/";

        private static bool isGetConfig;

        #region Public Methods
        public static void LogInByEmailPassword(string email, string password, Action<bool, LogInResponse, string, int> callback)
        {
            if (!isGetConfig)
                GetConfiguration();

            string url = uri + "account/signin-with-email-password";
            Request.EndPointCall<LogInResponse>(url, new LoginPayloadRequest()
            {
                email = email,
                password = password
            }, (success, data, message, code) =>
            {
                if (!success)
                    callback?.Invoke(false, new LogInResponse()
                    {
                        authToken = data.authToken,
                        email = data.email,
                        code = code,
                        message = message
                    }, message, code);
                else
                    callback?.Invoke(true, new LogInResponse()
                    {
                        authToken = data.authToken,
                        email = data.email,
                        code = code,
                        message = message
                    }, message, code);

            }, Request.RequestType.POST);
        }

        public static void LogInByAuthtoken(string authToken, Action<object> callback = null)
        {
            Debug.Log("Coming soon");
        }

        public static void GetUserDataByAuthToken(string authToken, Action<bool, UserDataResponse, string, int> callback)
        {
            if (!isGetConfig)
                GetConfiguration();

            string url = uri + "Security/user-data";

            Request.EndPointCall<UserDataResponse>(url, new GetUserDataByAuthTokenRequest()
            {
                AuthToken = authToken
            }, (success, data, message, code) =>
            {
                if (!success)
                    callback(false, data, message, code);
                else
                    callback(true, data, message, code);

            }, Request.RequestType.GET);
        }
        #endregion

        #region Private Methods
        private static void GetConfiguration()
        {
            PlatformConfig config = (PlatformConfig)AssetDatabase.LoadMainAssetAtPath("Assets/Packages/Platform/Resources/RequestConfig.asset");

            if (config != null)
            {
                uri = config.logInURL;
                // path = config.pathlogIn;
            }
            else
                Debug.LogWarning("Config Error. Not loaded");

            isGetConfig = true;
        }
        #endregion
    }

    #region Request Structs
    [Serializable]
    public struct LoginPayloadRequest
    {
        public string email;
        public string password;
    }
    [Serializable]
    public struct GetUserDataByAuthTokenRequest
    {
        public string AuthToken;
    }
    #endregion

    #region Response Structs
    [Serializable]
    public struct LogInResponse
    {
        public string message;
        public int code;
        public string email;
        public string authToken;
    }

    [Serializable]
    public struct UserDataResponse
    {
        public string userId;
        public string firstName;
        public string lastName;
        public string email;
        public bool emailVerified;
        public string photoUrl;
        public string lastLoginAt;
        public string createdAt;
    }
    #endregion
}
