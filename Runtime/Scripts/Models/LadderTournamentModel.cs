using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace PlatformRequest
{
    public static class LadderTournamentModel
    {
        private static string uri = "https://genesys-core-svc-dev.azurewebsites.net/Core/";

        private static bool isGetConfig;

        #region Public Methods
        /// <summary>
        /// Get current
        /// </summary>
        /// <param name="gameId"></param>
        /// <param name="callback"></param>
        public static void GetCurrentLadder(string gameId, Action<bool, LadderData, string, int> callback)
        {
            if (!isGetConfig)
                GetConfiguration();

            string url = uri + "current-ladder-tournament";

            Request.EndPointCall<LadderData>(url, new GetCurrentLadderRequest()
            {
                GameId = gameId
            }, (success, data, message, code) =>
            {
                if (!success)
                    callback(false, data, message, code);
                else
                    callback(true, data, message, code);

            }, Request.RequestType.GET);
        }

        public static void JoinTournamentLadder(string userId, string tournamentId, Action<bool, JoinToTournamentResponse, string, int> callback)
        {
            if (!isGetConfig)
                GetConfiguration();

            string url = uri + "JoinToTournament";

            Request.EndPointCall<JoinToTournamentResponse>(url, new JoinToTournamentLadderRequest()
            {
                userId = userId,
                tournamentId = tournamentId
            }, (success, data, message, code) =>
            {
                if (!success)
                    callback(false, data, message, code);
                else
                    callback(true, data, message, code);

            }, Request.RequestType.POST);
        }
        #endregion

        #region Private Methods
        private static void GetConfiguration()
        {
            PlatformConfig config = (PlatformConfig)AssetDatabase.LoadMainAssetAtPath("Assets/Packages/Platform/Resources/RequestConfig.asset");

            if (config != null)
                uri = config.tournamentURL;
            else
                Debug.LogWarning("Config Error. Not loaded");

            isGetConfig = true;
        }
        #endregion
    }

    #region Request Structs
    [Serializable]
    public struct GetCurrentLadderRequest
    {
        public string GameId;
    }
    [Serializable]
    public struct JoinToTournamentLadderRequest
    {
        public string userId;
        public string tournamentId;
    }
    #endregion

    #region Response Structs
    [Serializable]
    public struct JoinToTournamentResponse
    {
        public string userName;

    }
    [Serializable]
    public struct GetLadderDataResponse
    {
        public List<LadderData> rows;
    }
    [Serializable]
    public struct LadderData
    {
        public string tournamentId;
        public string gameId;
        public string name;
        public string startDate;
        public string endDate;
        public string frecuency;
        public string location;
        public int type;
        public string providerServerBuildId;
        public string providerMatchmakingQueueName;
        public int providerMatchmakingTeamSize;
        public int providerMatchmakingGiveUpTime;
        public int apiMaxAttempts;
        public int apiDelay;
    }
    #endregion
}