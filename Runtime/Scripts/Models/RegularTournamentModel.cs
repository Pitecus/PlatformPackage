using UnityEditor;
using UnityEngine;

namespace PlatformRequest
{
    public static class RegularTournamentModel
    {
        private static string uri = "https://genesys-security-svc-dev.azurewebsites.net/";

        private static bool isGetConfig;

        #region Public Methods
        #endregion

        #region Private Methods
        private static void GetConfiguration()
        {
            PlatformConfig config = (PlatformConfig)AssetDatabase.LoadMainAssetAtPath("Assets/Packages/Platform/Resources/RequestConfig.asset");

            if (config != null)
                uri = config.tournamentURL;
            else
                Debug.LogWarning("Config Error. Not loaded");

            isGetConfig = true;
        }
        #endregion
    }

    #region Request Structs
    #endregion

    #region Response Structs
    #endregion
}