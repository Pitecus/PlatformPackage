using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using UnityEngine;
using Newtonsoft.Json;

namespace PlatformRequest
{
    public static class Request
    {
        public static async void EndPointCall(string URL, object objectToSend, Action<bool, object> callback = null, RequestType type = RequestType.GET)
        {
            var _url = URL;
            StringContent data = null;
            if (objectToSend != null)
            {
                var json = JsonUtility.ToJson(objectToSend);
                data = new StringContent(json, Encoding.UTF8, "application/json");
                //Make GET parameters
                if (type == RequestType.GET)
                {
                    string paramsUrl = "";
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    foreach (KeyValuePair<string, object> entry in dictionary)
                    {
                        if (paramsUrl.Length > 0)
                            paramsUrl += "&";
                        paramsUrl += entry.Key + "=" + entry.Value;
                    }
                    //Set URL with parameters
                    _url += "?" + paramsUrl;
                }
            }
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = new HttpResponseMessage();
                switch (type)
                {
                    case RequestType.GET:
                        response = await httpClient.GetAsync(_url);
                        break;
                    case RequestType.POST:
                        response = await httpClient.PostAsync(_url, data);
                        break;
                    case RequestType.PUT:
                        response = await httpClient.PutAsync(_url, data);
                        break;
                    case RequestType.DELETE:
                        response = await httpClient.DeleteAsync(_url);
                        break;
                }
                string result = response.Content.ReadAsStringAsync().Result;
                ResponeBase<object> responseR = JsonUtility.FromJson<ResponeBase<object>>(result);
                if (!responseR.success)
                {
                    Debug.Log($"Request error: {responseR.message}");
                }
                if (callback != null)
                    callback(responseR.success, responseR.data);
            }
        }

        public static async void EndPointCall<T>(string URL, object objectToSend, Action<bool, T, string, int> callback = null, RequestType type = RequestType.GET)
        {
            var _url = URL;
            StringContent data = null;
            if (objectToSend != null)
            {
                var json = JsonUtility.ToJson(objectToSend);
                data = new StringContent(json, Encoding.UTF8, "application/json");
                //Make GET parameters
                if (type == RequestType.GET)
                {
                    string paramsUrl = "";
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    foreach (KeyValuePair<string, object> entry in dictionary)
                    {
                        if (paramsUrl.Length > 0)
                            paramsUrl += "&";
                        paramsUrl += entry.Key + "=" + entry.Value;
                    }
                    //Set URL with parameters
                    _url += "?" + paramsUrl;
                }
            }
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = new HttpResponseMessage();
                switch (type)
                {
                    case RequestType.GET:
                        response = await httpClient.GetAsync(_url);
                        break;
                    case RequestType.POST:
                        response = await httpClient.PostAsync(_url, data);
                        break;
                    case RequestType.PUT:
                        response = await httpClient.PutAsync(_url, data);
                        break;
                    case RequestType.DELETE:
                        response = await httpClient.DeleteAsync(_url);
                        break;
                }
                string result = response.Content.ReadAsStringAsync().Result;
                ResponeBase<T> responseR = JsonUtility.FromJson<ResponeBase<T>>(result);
                if (!responseR.success)
                {
                    Debug.Log($"Request error: {responseR}");
                }
                if (callback != null)
                    callback(responseR.success, responseR.data, responseR.message, responseR.statusCode);
            }
        }

        public static async void EndPointCallWithAll<T>(string URL, object objectToSend, Action<bool, ResponeBase<T>> callback = null, RequestType type = RequestType.GET)
        {
            var _url = URL;
            StringContent data = null;
            if (objectToSend != null)
            {
                var json = JsonUtility.ToJson(objectToSend);
                data = new StringContent(json, Encoding.UTF8, "application/json");
                //Make GET parameters
                if (type == RequestType.GET)
                {
                    string paramsUrl = "";
                    var dictionary = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);
                    foreach (KeyValuePair<string, object> entry in dictionary)
                    {
                        if (paramsUrl.Length > 0)
                            paramsUrl += "&";
                        paramsUrl += entry.Key + "=" + entry.Value;
                    }
                    //Set URL with parameters
                    _url += "?" + paramsUrl;
                }
            }
            using (var httpClient = new HttpClient())
            {
                HttpResponseMessage response = new HttpResponseMessage();
                switch (type)
                {
                    case RequestType.GET:
                        response = await httpClient.GetAsync(_url);
                        break;
                    case RequestType.POST:
                        response = await httpClient.PostAsync(_url, data);
                        break;
                    case RequestType.PUT:
                        response = await httpClient.PutAsync(_url, data);
                        break;
                    case RequestType.DELETE:
                        response = await httpClient.DeleteAsync(_url);
                        break;
                }
                string result = response.Content.ReadAsStringAsync().Result;
                ResponeBase<T> responseR = JsonUtility.FromJson<ResponeBase<T>>(result);
                if (!responseR.success)
                {
                    Debug.Log($"Request error: {responseR.message}");
                }
                if (callback != null)
                    callback(responseR.success, responseR);
            }
        }

        public enum RequestType
        {
            GET,
            POST,
            PUT,
            DELETE
        }
    }

    [Serializable]
    public class ResponeBase<T>
    {
        public int statusCode;
        public int messageType;
        public bool success;
        public string message;
        public T data;
    }
}