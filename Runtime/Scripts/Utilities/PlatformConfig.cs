using UnityEngine;

namespace PlatformRequest
{
    [CreateAssetMenu(fileName = "RequestConfig", menuName = "Request/Config")]
    public class PlatformConfig : ScriptableObject
    {
        [Header("Login")]
        public string logInURL;
        [Header("Register")]
        public string registerURL;
        [Header("Tournament")]
        public string tournamentURL;
        [Header("Test")]
        public bool testing;
    }
}
